import numpy as np
import matplotlib.pyplot as plt
import emcee
from ase.io import read

from hiphive import ClusterSpace, StructureContainer
from hiphive.fitting import Optimizer
from hiphive.utilities import get_displacements


def read_dft_dir(dft_dir):
    atoms = read(dft_dir + '/POSCAR')
    forces = np.loadtxt(dft_dir + '/forces.txt')
    return atoms, forces


# parameters
cutoffs = [8.5]
dim = 6

# setup training structure
prim = read('structures/POSCAR_prim_accurate')
supercell_ideal = read('structures/POSCAR_dim{}_ideal'.format(dim))
supercell_dft, forces = read_dft_dir('dft_calculations/POSCAR_dim6_rattle_ind0')

displacements = get_displacements(supercell_dft, supercell_ideal)
max_disp = np.linalg.norm(displacements, axis=1).max()
assert max_disp < 0.5

atoms = supercell_ideal.copy()
atoms.new_array('displacements', displacements)
atoms.new_array('forces', forces)

# setup SC
cs = ClusterSpace(prim, cutoffs)
sc = StructureContainer(cs)
sc.add_structure(atoms)

# fit data
A, y = sc.get_fit_data()
n_rows, n_cols = A.shape

# train OLS FCP
opt = Optimizer((A, y), train_size=1.0)
opt.train()
print(opt)
parameters_ols = opt.parameters


# Bayesian sampling of models
# ==========================
# We use very simple priors and posterior to simply demonstrate the procedure


def log_likelihood(theta):
    """
    theta is the solution vector
    """
    y_predicted = np.dot(A, theta)
    return -0.5 * np.sum(np.log(2 * np.pi * dy ** 2) + (y - y_predicted) ** 2 / dy ** 2)


def log_gaussian_prior(theta):
    """
    Return a gaussian with width std
    """
    return -0.5 * np.sum(np.log(2 * np.pi * std ** 2) + theta ** 2 / std ** 2)


def log_posterior(theta):
    return log_gaussian_prior(theta) + log_likelihood(theta)


# bayesian parameters
dy = 0.006  # std in likelihood
std = 1.0   # std in gaussian priors


# MCMC parameters
n_walkers = 480   # number of MCMC walkers
n_every = 10      # keep every x:th sample
n_steps = 5000 / n_every  # steps per walker

# starting points for MCMC, based on OLS for quicker convergence
theta_start = parameters_ols + np.random.normal(0, 0.01, (n_walkers, n_cols))

# sample
sampler = emcee.EnsembleSampler(n_walkers, n_cols, log_posterior)

sampler.run_mcmc(theta_start, n_steps, progress=True, thin_by=n_every)
samples_chain = sampler.chain
print(samples_chain.shape)


# Select models from MCMC trajectory
n_models = 200
n_eq = 100
samples = samples_chain[:, n_eq:, :].reshape((-1, n_cols))
model_inds = np.random.choice(samples.shape[0], n_models, replace=False)
ensemble_parameters = samples[model_inds]

np.savetxt('data/MCMC_parameters.txt', ensemble_parameters)


# plot parameter trajectory for a few walkers
n_params_to_plot = 10
inds_to_plot = np.argsort(np.abs(parameters_ols))[::-1][0:n_params_to_plot]
walker_inds = [0, 1]
fig = plt.figure(figsize=(13, 5.2))
for i, walker_ind in enumerate(walker_inds, start=1):
    ax1 = fig.add_subplot(1, len(walker_inds), i)
    ax1.plot(samples_chain[walker_ind, :, inds_to_plot].T, alpha=0.7)
    ax1.set_title('Walker 0')
    ax1.set_xlabel('iteration')
    ax1.set_ylabel('Parameters')
fig.tight_layout()


# plot combined samples from all walkers
fig = plt.figure(figsize=(7, 5.2))
plt.plot(samples[:, inds_to_plot], alpha=0.5)
plt.xlabel('iteration')
plt.ylabel('Parameters')
fig.tight_layout()
plt.show()


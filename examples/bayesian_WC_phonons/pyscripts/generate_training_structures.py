from ase.io import read, write
from hiphive.structure_generation import generate_rattled_structures


# parameters
n_structures = 1
rattle_std = 0.01
dim = 6

# setup
prim = read('structures/POSCAR_prim_accurate')
supercell = prim.repeat(dim)

# generate rnd structures
structures = generate_rattled_structures(supercell, n_structures, rattle_std)

# write structures

write('structures/POSCAR_dim{}_ideal'.format(dim), supercell, format='vasp', sort=True)

for i, structure in enumerate(structures):
    fname = 'structures/POSCAR_dim{}_rattle_ind{}'.format(dim, i)
    write(fname, structure, format='vasp', sort=True)

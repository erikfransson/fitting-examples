import numpy as np
from phonopy import Phonopy
from phonopy.structure.atoms import PhonopyAtoms
from ase import Atoms


def get_band(q_start, q_stop, N):
    """ Return path between q_start and q_stop """
    q_start = np.array(q_start)
    q_stop = np.array(q_stop)
    return np.array([q_start + (q_stop-q_start)*i/(N-1) for i in range(N)])


def get_bands(Nq=100):
    bands = []
    bands.append(get_band([0, 0, 0.0], [0, 0, 1/2], Nq))        # G2A
    bands.append(get_band([0, 0, 1/2], [1/3, 1/3, 1/2], Nq))    # A2H
    bands.append(get_band([1/3, 1/3, 1/2], [1/3, 1/3, 0], Nq))  # H2K
    bands.append(get_band([1/3, 1/3, 0], [0, 0, 0], Nq))        # K2G
    return bands


def setup_phonopy(prim, dim):
    atoms_phonopy = PhonopyAtoms(symbols=prim.get_chemical_symbols(),
                                 scaled_positions=prim.get_scaled_positions(),
                                 cell=prim.cell)
    phonopy = Phonopy(atoms_phonopy, supercell_matrix=dim*np.eye(3), primitive_matrix=None)
    return phonopy


def get_phonopy_supercell(prim, dim):
    phonopy = setup_phonopy(prim, dim)
    supercell = phonopy.get_supercell()
    supercell = Atoms(cell=supercell.cell, numbers=supercell.numbers, pbc=True,
                      scaled_positions=supercell.get_scaled_positions())
    return supercell


def get_dispersion(prim,  dim, fcm):
    phonopy = setup_phonopy(prim, dim)
    fcs = fcm.get_force_constants()
    fc2 = fcs.get_fc_array(order=2)
    phonopy.set_force_constants(fc2)

    # get phonon dispersion
    bands = get_bands()
    phonopy.set_band_structure(bands)
    qvecs, qnorms, freqs, _ = phonopy.get_band_structure()
    q = np.hstack(qnorms)
    f = np.vstack(freqs)
    return q, f
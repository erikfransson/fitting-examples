from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
from hiphive.force_constant_model import ForceConstantModel
from hiphive.fitting import Optimizer
from hiphive.utilities import get_displacements
from tools import get_dispersion, get_phonopy_supercell

# parameters
dim = 6
THz2meV = 4.13567


# setup
supercell = get_phonopy_supercell(prim, dim)
fcm = ForceConstantModel(supercell, cs)
fcm.parameters = parameters_ols
q_ols, f_ols = get_dispersion(prim, dim, fcm)


## Bagging test
from hiphive.fitting import EnsembleOptimizer
n_models = 20
eopt = EnsembleOptimizer((A, y), ensemble_size=n_models)
eopt.train()

# compute all  dispersions
freqs_all = []
for i, p in enumerate(eopt.parameters_splits):
    fcm.parameters = p
    q, f = get_dispersion(prim, dim, fcm)
    plt.plot(q, f, '-k', alpha=0.5)
    freqs_all.append(f)
freqs_all = np.array(freqs_all)

plt.show()

# plot bagging dispersion
plt.figure()
freqs_ave = freqs_all.mean(axis=0)
freqs_std = freqs_all.std(axis=0)


plt.plot(q, freqs_ave, '-k')
plt.plot(q_ols, f_ols, '-r')

for i in range(freqs_ave.shape[1]):
    fmin = freqs_ave[:, i] - 4*freqs_std[:, i]
    fmax = freqs_ave[:, i] + 4*freqs_std[:, i]
    line = plt.fill_between(q, fmin, fmax, color='k', alpha=0.2)

plt.show()

asd

import numpy as np
import matplotlib.pyplot as plt
import mplpub
from collections import defaultdict
from sklearn.datasets import make_sparse_coded_signal
from hiphive.fitting import Optimizer

mplpub.setup(template='phd_fw', width=5.8, height=5.0)


# parameters
n_rows = 120
n_cols = 101
n_nonzero_coefs = 10
seed = 2030
noise_amplitude = 0.04

# generate sparse data
y, A, p_ref = make_sparse_coded_signal(n_samples=1,
                                       n_components=n_cols,
                                       n_features=n_rows,
                                       n_nonzero_coefs=n_nonzero_coefs,
                                       random_state=seed)
assert np.allclose(A.dot(p_ref) - y, 0)

# noise
y_samples = y + np.random.normal(0, noise_amplitude, (n_rows))

kwargs = defaultdict(dict)
cv = 30
alphas = np.logspace(-3, -1, 200)
kwargs['ardr'] = dict(line_scan=True, cv_splits=cv)
kwargs['lasso'] = dict(max_iter=1500000, tol=1e-6, cv=cv, alphas=alphas)
kwargs['rfe'] = dict(cv_splits=cv)
kwargs['omp'] = dict(cv_splits=cv)


fit_methods = ['least-squares', 'lasso', 'omp', 'rfe', 'ardr', 'adaptive-lasso']
fit_methods = ['least-squares', 'lasso', 'rfe']

data = dict()
data['Truth'] = p_ref
for fm in fit_methods:
    opt = Optimizer((A, y_samples), fit_method=fm, **kwargs[fm], train_size=1.0)
    opt.train()
    print(opt)
    data[fm] = opt.parameters

# plot
color1 = 'black'
color2 = 'tab:red'

fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.plot(p_ref, '-')
ax1.plot(data['least-squares'], '-')
ax1.plot(data['lasso'], '-')
ax1.plot(data['rfe'], '-')


# plot params
xlim = [0, n_cols-1]
ylim = [-1.4, 1.4]
yticks = np.arange(-10, 10, 1)
labels = dict()
labels['least-squares'] = 'OLS'
labels['lasso'] = 'LASSO'
labels['rfe'] = 'RFE'
labels['Truth'] = 'Truth'
labels['ardr'] = 'ARDR'
labels['omp'] = 'OMP'


# plotting
yscale = 1.5
fig = plt.figure()
gs = plt.GridSpec(nrows=len(data), ncols=1, hspace=0)

for i, (fm, parameters) in enumerate(data.items(), start=0):
    ax = plt.subplot(gs[i])
    ax.stem(parameters/yscale, basefmt='-k')

    ax.set_yticks(yticks)
    if i != len(data)-1:
        ax.set_xticks([])
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

    x = 0.035
    y = 0.8
    ax.text(x, y, labels[fm], transform=ax.transAxes)

ax.set_xlabel('Parameter index')

fig.tight_layout()
fig.savefig('pdf/feature_selection_demo.pdf')
plt.show()

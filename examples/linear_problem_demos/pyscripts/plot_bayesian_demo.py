import numpy as np
import matplotlib.pyplot as plt
import emcee
import mplpub

mplpub.setup(template='phd_fw', width=5.8, height=3)


def log_gaussian_prior1(theta, alpha):
    """
    Return a gaussian with width std=alpha
    """
    return -0.5 * np.sum(np.log(2 * np.pi * alpha ** 2) + theta ** 2 / alpha ** 2)


def log_likelihood(A, y_target, sigma, theta):
    y_predicted = np.dot(A, theta)
    return -0.5 * np.sum(np.log(2 * np.pi * sigma ** 2) + (y_target - y_predicted) ** 2 / sigma ** 2)


def log_posterior(theta, A, y):

    # get params
    p = theta    # ecis
    alpha = 1
    sigma = 0.04

    # likelihood
    log_L = log_likelihood(A, y, sigma, p)

    # prior for params
    log_prior_ecis = log_gaussian_prior1(p, alpha)

    return log_L + log_prior_ecis


def function(x):
    return np.polyval(p_ref, x)


# parameters
deg = 8
n_samples = 10
noise_std = 0.045


# generate data
seed = 129
np.random.seed(seed)
p_ref = [0, 0, 0, 0, 0, 0.15, 0.2, -0.1, 0.05]
p_ref = 3 * np.array(p_ref)
assert len(p_ref) == deg + 1

x_samples = np.random.uniform(-1, 1, n_samples)
noise = np.random.normal(0, noise_std, n_samples)
y_samples = function(x_samples) + noise


# construct poly fit matrix
A_poly = np.zeros((n_samples, deg+1))
for i, d in enumerate(reversed(range(deg + 1))):
    col = x_samples ** d
    A_poly[:, i] = col


# MCMC sampling
n_walkers = 20   # number of MCMC walkers
n_steps = 10000  # steps per walker
n_cols = deg + 1

# starting positions
theta_start = np.random.normal(0, 0.1, (n_walkers, n_cols))  # params


# sample posterior
sampler = emcee.EnsembleSampler(n_walkers, n_cols, log_posterior, args=[A_poly, y_samples])
sampler.run_mcmc(theta_start, n_steps, progress=True)
samples_chain = sampler.chain
ln_post = sampler.lnprobability

# draw a few models from MCMC run
n_models = 500
n_eq = 500
samples = samples_chain[:, n_eq:, :].reshape(-1, 9)
inds = np.random.choice(samples.shape[0], n_models, replace=False)
parameter_samples = samples[inds]


# construct poly fit matrix
N = 1000
x_lin = np.linspace(-1.1, 1.1, N)
A_lin = np.zeros((N, deg+1))
for i, d in enumerate(reversed(range(deg + 1))):
    col = x_lin ** d
    A_lin[:, i] = col


# make predictions for all models
y = []
for p in parameter_samples:
    y.append(A_lin.dot(p))
y = np.array(y)

y_ave = np.mean(y, axis=0)
y_std = np.std(y, axis=0)

# # Try to make posterior predictive which includes likelihood (noise)
# data = []
# for p in parameter_samples:
#     y = []
#     for _ in range(100):
#         y_i = A_lin.dot(p) + np.random.normal(0, 0.04)
#         y.append(y_i)
#     data.append(y)
# data = np.array(data).reshape(-1, N)

# y_ave2 = np.mean(data, axis=0)
# y_std2 = np.std(data, axis=0)

# y_ave = y_ave2
# y_std = y_std2

# plot params
color1 = 'black'
color2 = 'tab:red'
color2 = 'tab:blue'

xlim = [-1.0, 1.0]
ylim = [0.0, 0.3]
ylim = [0.0, 1]

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)

# indiviual lines
if False:
    lw = 0.7
    alpha = 0.5
    n_lines = 10
    ax1.plot(x_lin, y[0], '-', color=color2, alpha=alpha, lw=lw, label='Models')
    for i in range(1, n_lines):
        ax1.plot(x_lin, y[i], '-', color=color2, alpha=alpha, lw=lw)

l2 = ax1.fill_between(x_lin, y_ave-2 * y_std, y_ave+2 * y_std, alpha=0.25, color=color2,
                      lw=0)
l1 = ax1.plot(x_lin, y_ave, color=color2, label='Average')

l3 = ax1.plot(x_lin, function(x_lin), '-', color=color1, label='True model')
l4 = ax1.plot(x_samples, y_samples, 'o', color=color1, label='Samples')

labs = ['Average prediction', '$\pm$ 2 Standard deviations', 'True model', 'Samples']
ax1.legend([l1[0], l2, l3[0], l4[0]], labs, loc=9)
ax1.set_xlim(xlim)
ax1.set_ylim(ylim)
ax1.set_xlabel('x-data')
ax1.set_ylabel('y-data')

fig.tight_layout()
fig.savefig('pdf/bayesian_demo.pdf')
plt.show()

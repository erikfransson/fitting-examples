import numpy as np
import matplotlib.pyplot as plt
from hiphive.fitting import Optimizer
import mplpub
mplpub.setup(template='phd_fw', width=5.8, height=3)


def function(x):
    return np.polyval(p_ref, x)


# parameters
deg = 8
n_samples = 10
noise_std = 0.045


# generate data
seed = 129
np.random.seed(seed)
p_ref = [0, 0, 0, 0, 0, 0.15, 0.2, -0.1, 0.05]
p_ref = 3 * np.array(p_ref)

x_samples = np.random.uniform(-1, 1, n_samples)
noise = np.random.normal(0, noise_std, n_samples)
y_samples = function(x_samples) + noise
x_lin = np.linspace(-1.1, 1.1, 10000)

# construct poly fit matrix
A_poly = np.zeros((n_samples, deg+1))
for i, d in enumerate(reversed(range(deg + 1))):
    col = x_samples ** d
    A_poly[:, i] = col

# fit l2
opt = Optimizer((A_poly, y_samples), fit_method='least-squares', train_size=1.0)
opt.train()
p_l2 = opt.parameters

# ridge fit alpha = 0.1
alpha_vals = np.linspace(0.01, 0.5, 100)
opt = Optimizer((A_poly, y_samples), fit_method='ridge', train_size=1.0, alphas=alpha_vals)
opt.train()
print(opt)
p_ridge = opt.parameters


# LASSO fit alpha = 0.1
alpha_vals = np.linspace(0.001, 0.01, 100)
opt = Optimizer((A_poly, y_samples), fit_method='lasso', train_size=1.0, tol=1e-5,
                alphas=alpha_vals, max_iter=50000)
opt.train()
print(opt)
p_lasso = opt.parameters


print(p_l2)
print(p_ridge)
print(p_lasso)

# plot
color1 = 'black'
color2 = 'tab:red'
color3 = 'tab:green'
color4 = 'tab:blue'
xlim = [-1.0, 1.0]
ylim = [0.0, 0.3]
ylim = [0.0, 1]

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)

ax1.plot(x_lin, np.polyval(p_l2, x_lin), '-', color=color2, label='OLS')
ax1.plot(x_lin, np.polyval(p_ridge, x_lin), '-', color=color3, label=r'Ridge')
ax1.plot(x_lin, np.polyval(p_lasso, x_lin), '-', color=color4, label=r'LASSO')
ax1.plot(x_lin, function(x_lin), '-', color=color1, label='True model')
ax1.plot(x_samples, y_samples, 'o', color=color1, label='Samples')

ax1.legend(loc=9)
ax1.set_xlim(xlim)
ax1.set_ylim(ylim)
ax1.set_xlabel('x-data')
ax1.set_ylabel('y-data')

fig.tight_layout()
fig.savefig('pdf/regularization_demo.pdf')
plt.show()
